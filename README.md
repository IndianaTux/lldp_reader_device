# LLDP based port identifier device

This device is based on the following blog post: http://reloadin10.blogspot.com/2014/03/arduino-lldp-viewer.html

## Compiling and uploading using the Arduino IDE

First install the following library dependancies

- Newliquidcrystal

  - Download the `NewLiquidcrystal` library archive from https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads/Newliquidcrystal_1.3.5.zip
  - In the Arduino IDE  on in Sketch -> Include Library -> Add ZIP Library)
  - Select the download file to load it in the Arduino IDE

- Ethernet v1.1.2

  _You must use the version v1.1.2 as the newer version of this library is not compatible with the Arduino code_

  - Download the `Ethernet` v1.1.2 library archive from https://github.com/arduino-libraries/Ethernet/archive/1.1.2.zip
  - In the Arduino IDE  on in Sketch -> Include Library -> Add ZIP Library)
  - Select the download file to load it in the Arduino IDE

Then compile and upload as usual

## Compiling with platform.io

Just `cd` to `arduino_src` and run `platformio run`. To upload run `platformio run -t upload`

## Bill of material

- Arduino Uno (~18.42$)
  - https://www.robotshop.com/ca/en/sunfounder-uno-usb-microcontroller-r3.html
- W5100 Ethernet shield (~21.04$)
  - https://www.robotshop.com/ca/en/w5100-ethernet-shield.html
- i2c 16x2 LCD (~13.68$)
  - https://www.robotshop.com/ca/en/dfrobot-i2c-twi-lcd1602-module.html
- 9v battery adapter (~1.32$)
  - https://www.robotshop.com/ca/en/9v-battery-bh-06.html
- Miniature on-off switch (~1.48$)
  - https://abra-electronics.com/electromechanical/switches/toggle-switches/sw101-miniature-toggle-switch-spst-on-off-6a-125v-sw101.html
- 3D printed enclosure
- Screws and nuts
  - 2x 3mm x 10mm screw
  - 2x 3mm nut
  - 1x 4-40 x 1" screw
  - 1x 4-40 nut
  - 4x 2mm x 16 screw
  - 4x 2mm nut
  - 10x 3mm x 6mm screw
